package com.teste.suntech.spring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.teste.suntech.spring.model.User;
import com.teste.suntech.spring.service.UserService;

@Controller
public class UserController {
	private UserService userService;
	
	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setuserService(UserService us){
		this.userService = us;
	}
	
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("userList", this.userService.userList());
		return "user";
	}
	
	@RequestMapping(value= "/user/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") User u){
		
		if(u.getId() == 0){
			this.userService.addUser(u);
		}else{
			this.userService.updateUser(u);
		}
		return "redirect:/users";
	}
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
        this.userService.removeUser(id);
        return "redirect:/users";
    }
	
	 @RequestMapping("/edit/{id}")
	    public String editPerson(@PathVariable("id") int id, Model model){
	        model.addAttribute("person", this.userService.getUserById(id));
	        model.addAttribute("listPersons", this.userService.userList());
	        return "user";
	    }

}
