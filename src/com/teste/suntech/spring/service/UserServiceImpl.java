package com.teste.suntech.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import com.teste.suntech.spring.dao.UserDAO;
import com.teste.suntech.spring.model.User;

public class UserServiceImpl implements UserService{
	
	private UserDAO userDao;
	
	public void setUserDAO(UserDAO userDao) {
		this.userDao = userDao;
	}

	@Override
	@Transactional
	public void addUser(User u) {
		this.userDao.addUser(u);
		
	}

	@Override
	@Transactional
	public void updateUser(User u) {
		this.userDao.updateUser(u);
	}

	@Override
	@Transactional
	public List<User> userList() {
		return this.userDao.listUser();
	}

	@Override
	@Transactional
	public User getUserById(int id) {
		this.userDao.getUserById(id);
		return null;
	}

	@Override
	@Transactional
	public void removeUser(int id) {
		this.userDao.removeUser(id);
		
	}

}
