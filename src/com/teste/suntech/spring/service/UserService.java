package com.teste.suntech.spring.service;

import java.util.List;

import com.teste.suntech.spring.model.User;

public interface UserService {
	public void addUser(User p);
	public void updateUser(User p);
	public List<User> userList();
	public User getUserById(int id);
	public void removeUser(int id);
	
	
}
