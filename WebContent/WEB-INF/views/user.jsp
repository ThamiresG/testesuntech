<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spr
ing" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Teste Suntech</title>
</head>
<body>
	<h1>
  Add a user
</h1>
<c:url var="addAction" value="/user/add"></c:url>
	<form:form action="${addAction}" commandName="user">
	  <table>
	    <c:if test="${!empty user.username}">
	      <tr>
	        <td>
	          <form:label path="id">
	            <spring:message text="ID" />
	          </form:label>
	        </td>
	        <td>
	          <form:input path="id" readonly="true" size="8" disa bled="true" />
	          <form:hidden path="id" />
	        </td>
	      </tr>
	    </c:if>
	    <tr>
	      <td>
	        <form:label path="username">
	          <spring:message text="UserName" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="username" />
	      </td>
	    </tr>
	    <tr>
	      <td>
	        <form:label path="password">
	          <spring:message text="Password" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="password" />
	      </td>
	    </tr>
	
	    <tr>
	      <td>
	        <form:label path="is_enabled">
	          <spring:message text="Isenabled" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="is_enabled" />
	      </td>
	    </tr>
	
	    <tr>
	      <td>
	        <form:label path="register_date">
	          <spring:message text="Data de Registro" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="register_date" />
	      </td>
	    </tr>
	
	    <tr>
	      <td>
	        <form:label path="name">
	          <spring:message text="Name" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="name" />
	      </td>
	    </tr>
	    <tr>
	      <td>
	        <form:label path="surname">
	          <spring:message text="Surname" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="surname" />
	      </td>
	    </tr>
	    <tr>
	      <td>
	        <form:label path="email">
	          <spring:message text="Email" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="email" />
	      </td>
	    </tr>
	    <tr>
	      <td>
	        <form:label path="telefone">
	          <spring:message text="Telefone" />
	        </form:label>
	      </td>
	      <td>
	        <form:input path="telefone" />
	      </td>
	    </tr>
	    <tr>
	      <td colspan="2">
	        <c:if test="${empty user.name}">
	          <input type="submit" value="<spring:message text=" Add user "/>" />
	        </c:if>
	      </td>
	    </tr>
	  </table>
	</form:form>
	<h3>User List</h3>
	<c:if test="${!empty userList}">
	  <table class="tg">
	    <tr>
	      <th width="80">ID</th>
	      <th width="120">User Name</th>
	      <th width="60">Password</th>
	      <th width="120">Enable?</th>
	      <th width="120">Data de Registro</th>
	      <th width="120">Name</th>
	      <th width="120">Sobrenome</th>
	      <th width="120">Email</th>
	      <th width="60">Telefone</th>
	      <th width="60">Action</th>
	    </tr>
	    <c:forEach items="${userList}" var="user">
	      <tr>
	        <td>${user.id}</td>
	        <td>${user.username}</td>
	        <td>${user.password}</td>
	        <td>${user.is_enabled}</td>
	        <td>${user.register_date}</td>
	        <td>${user.name}</td>
	        <td>${user.surname}</td>
	        <td>${user.email}</td>
	        <td>${user.telefone}</td>
	        <a href="<c:url value='/remove/${user.id}' />" cla ss="btn">Delete</a></td>
	      </tr>
	    </c:forEach>
	  </table>
	</c:if>
</body>
</html>